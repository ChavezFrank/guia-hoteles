$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});
$(function () {
    $('.carousel').carousel({
        interval: 2000,
    });
});
$(function () {
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('El modal se inicio');
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-outline-success');
    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('El modal se mostro');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('El modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('El modal se oculto');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
    });
});